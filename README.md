# Rythme Front

#### Proyecto Final del Bootcamp Full Stack Developer

Este ha sido un proyecto en equipo en el que he participado junto con mis compañeros Alvaro del Castillo Villalba y Alejandro Abel Rodriguez da Silva

##### Levantar el proyecto

1. Entrar en rythme-front
2. Instalar dependencias: npm i
3. Levantar el proyecto con el comando npm run start
4. Automáticamente se abrirá el proyecto en el navegador prederminado ó también poniendo directamente en el navegador http://localhost:3000/


Este proyecto está realizado con React y también hemos implementado Redux

Para los estilos hemos empleado metodología BEM y SASS y está diseñado para mobile

En el proyecto hemos instalado también las siguientes librerías:
 - Lottie
 - Qr. Code
 - Leaflet

El proyecto está desplegado en netlify -> https://thebestrythme.netlify.app/


La parte back está creada con NODE en el siguiente proyecto https://gitlab.com/Mariamhzd/rythme-back y está desplegada en Heroku

##### Proyecto

El proyecto es una App para comprar entradas para conciertos.





