import React from 'react';
import { useSelector } from 'react-redux';
import ProfileCard from '../../components/ProfileCard/ProfileCard';
import { Link } from 'react-router-dom';
import { selectUser } from '../../reducers/userSlice';


import './UserProfile.scss';

   const UserProfile = () => {
      const user = useSelector(selectUser);

      return (
         <div className="profile">
         <div id="empty1"></div>
            <div id="pcontainer">
               <ProfileCard className="pcontainer__card" user={ user }/>
               <div className="container_button">
                  <Link to="/settings/profile/edit">
                     <button id="button">Editar</button>
                  </Link>
               </div>
            </div>
            <div id="empty2"></div>
         </div>
      );
   }

export default UserProfile;