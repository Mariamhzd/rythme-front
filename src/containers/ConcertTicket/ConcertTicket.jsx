import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { withRouter } from 'react-router-dom';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MapView from "../../componentsMap/MapView";
import Home from "../../componentsMap/Home";



import "./ConcertTicket.scss";


const ConcertTicket = ({ match }) => {

  const [ticket, setTicket] = useState({})

  useEffect(() => {
    fecthTicket()
  }, [match.params.id])

  const fecthTicket = async () => {
    const response = await fetch(`https://rithme.herokuapp.com/concert/${match.params.id}`)
    const buyticket = await response.json()
    setTicket(buyticket)
  }


  return (
    <div className="ticket">
      <div className="ticket--card">
        <img className="ticket--card__image" src={ticket.concert && ticket.concert.artist[0].picture} alt="concer hall imagen" />
        <p className="ticket--card__date">{ticket.concert && ticket.concert.date}</p>
      </div>
      <div className="ticket--info">
        <h2 className="ticket--info__artist">{ticket.concert && ticket.concert.artist[0].name}</h2>
        <p className="ticket--info__room">en {ticket.concert && ticket.concert.concertHall[0].name}</p>
        <p className="ticket--info__location">{ticket.concert && ticket.concert.concertHall[0].location}, {ticket.concert && ticket.concert.concertHall[0].city}</p>
      </div>
      <div className="ticket--map">
        <Router>
          <Switch>
            <Route path="/map">
              <MapView />
            </Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Router>
      </div>
      <div className="ticket--buy">
        <h3 className="ticket--buy__price">€ {ticket.concert && ticket.concert.price}</h3>
        <button className="ticket--buy__button">
          <Link to={`/ticket/${match.params.id}/pago`}>
            Comprar ahora
          </Link></button>
      </div>
    </div>
  )
}

export default withRouter(ConcertTicket);