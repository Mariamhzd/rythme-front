import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { selectTicket, myTicketList } from "../../reducers/ticketSlice";
// import { selectUser } from "../../reducers/userSlice";

import './MyConcerts.scss';

const MyConcerts = (props) => {
    const dispatch = useDispatch();
    const mytickets = useSelector(selectTicket);
    // const [myConcertsList, setMyConcertList] = useState([]);

    const blah = async () => {
        await dispatch(myTicketList());

        //  setMyConcertList(mytickets);
    }
    useEffect(() => {
        blah();


    }, [])


    return (
        <div className="mytickets">
            {
                mytickets.ticket && Array.isArray(mytickets.ticket) && mytickets.ticket.map(ticket => (
                    <div className="mytickets-card" key={JSON.stringify(ticket)}>
                        <img className="mytickets-card__img" src={ticket.singer.picture} alt="" width="150px"></img>

                        <div className="mytickets-date">
                            <h2 className="mytickets-card__name">{ticket.singer.name}</h2>
                            <p className="mytickets-card__date">{ticket.date}</p>
                            <p className="mytickets-card__hour">{ticket.hour}</p>
                            <p className="mytickets-card__price">{ticket.price} €</p>
                        </div>
                    </div>
                ))
            }
        </div>
    )
}

export default MyConcerts;