import React from 'react'
import { withRouter } from "react-router-dom";
import { Link } from "react-router-dom";

import "./MenuButtons.scss"

export const MenuButtons = () => {
    return (
        <div className="special--buttoms">
            <button className="btn rounded">
                <Link to="/Chat">
                <span className="text-green">
                    Amigos
                </span>
                </Link>
            </button>
            <button className="btn rounded">
                <Link to="/fanclub">
                <span className="text-green">
                    Fan Club
                </span>
                </Link>
            </button>
        </div>  
    )
}

export default withRouter(MenuButtons);
