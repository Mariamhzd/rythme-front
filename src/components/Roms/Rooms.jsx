import React from 'react';

import "./Rooms.scss"

const Rooms = () => {

    return (
        <div className="sincronice">
            <h3 className="title">MIRA TUS SALAS FAVORITAS</h3>
            <div className="streaming">
                <div className="space" >
                    <div className="streaming-music2">
                        <a href="https://www.salaclamores.es/"><img src="https://pbs.twimg.com/profile_images/776755674877485056/JRhS94jD.jpg" alt="" className="streaming-music__image" ></img></a>
                    </div>
                    <div className="streaming-music2">
                        <a href="https://play.google.com/store/apps/details?id=com.google.android.music&hl=es&gl=US"><img src="https://yt3.ggpht.com/ytc/AAUvwniIr9pRSwKnP3HAPDJhpzTcLgf6qNAS9yucMsR90Q=s900-c-k-c0x00ffffff-no-rj" alt="logo google play" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music2">
                        <a href="https://www.amazon.es/music/prime"><img src="https://verostec.com/images/logoriviera.jpg" alt="amazon music logo" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music2">
                        <a href="https://soundcloud.com/"><img src="https://pbs.twimg.com/profile_images/1275809648927531010/7_A1HrYb_400x400.jpg" alt="soundcloud logo" className="streaming-music__image" ></img></a>
                    </div>
                </div>
                <div div className="space">
                    <div className="streaming-music2">
                        <a href="https://www.spotify.com/es/home/"><img src="https://www.madrid-open.com/wp-content/uploads/2015/11/home_lacajamagica-1.jpg" alt="logo spotify" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music2">
                        <a href="https://music.apple.com/us/browse"><img src="https://s3.eu-central-1.amazonaws.com/images.jacksonlive.es/upload/spots/1x1/1351587492277.jpg" alt="logo apple music" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music2">
                        <a href="https://www.deezer.com/es/"><img src="https://discotecasgratis.com/img/uploads/Property/67/PropertyPicture/large/1568190236_kapital-2-3.jpg" alt="logo deezer" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music2">
                        <a href="https://tidal.com/"> <img src="https://houseandujar.files.wordpress.com/2020/03/1570007395_190216_madrid_fabrik_150_003.jpg" alt="tidal logo" className="streaming-music__image" ></img></a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Rooms;


