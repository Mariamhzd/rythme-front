import "leaflet/dist/leaflet.css";

import React, { useState, useEffect } from "react";
import { Map, TileLayer } from "react-leaflet";
import data from "../assets/data.json";
import Markers from "./VenueMarkers";
import L from "leaflet"
import { useLocation, useHistory } from "react-router-dom";

import "./MapView.scss"

const MapView = (props) => {
  const [state, setState] = useState({
    currentLocation: L.latLng[{ lat: 52.52437, lng: 13.41053 }],
    zoom: 13,
    data,
  });

  const location = useLocation();
  const history = useHistory();

  useEffect(() => {
    if (location.state.latitude && location.state.longitude) {
      const currentLocation = {
        lat: location.state.latitude,
        lng: location.state.longitude,
      };
      setState({
        ...state,
        data: {
          salas: state.data.salas.concat({
            name: "new",
            geometry: [currentLocation.lat, currentLocation.lng],
          }),
        },
        currentLocation,
      });
      history.replace({
        pathname: "/map",
        state: {},
      });
    }
  }, [location]);

  return (
    <Map className="pepe" center={state.currentLocation} zoom={state.zoom}>
      <TileLayer
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Markers salas={state.data.salas} />
    </Map>
  )
}

export default MapView;

