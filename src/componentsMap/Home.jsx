import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";

import "./Home.scss";

const Home = () => {
  const [state, setState] = useState({
    longitude: 0,
    latitude: 0,
  });

  useEffect(() => {
    navigator.geolocation.getCurrentPosition(
      function (position) {
        setState({
          longitude: position.coords.longitude,
          latitude: position.coords.latitude,
        });
      },
      function (error) {
        console.error("Error Code = " + error.code + " - " + error.message);
      },
      {
        enableHighAccuracy: true,
      }
    );
  }, []);

  return (
    <div className="map">
      <p className="map__latlong">Latitude: {state.latitude}</p>
      <p className="map__latlong">longitude: {state.longitude}</p>
      <Link className="map--link"
        to={{
          pathname: "/map",
          state,
        }}
      >
        Ver en el mapa
      </Link>
    </div>
  );
};

export default Home;
